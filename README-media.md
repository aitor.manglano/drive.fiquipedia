Aparte de almacenar ficheros, la mayoría pdf, se comenta aquí la idea de almacenar aquí algunos recursos multimedia: en FiQuiPedia antes de la migración de 2021 había pocas imágenes almacenadas localmente, casi siempre se enlaza a la fuente. Eso lo hace más dependiente de que la fuente mantenga el recurso, pero pasa en general con muchos recursos. 

En general se cuida el [licenciamiento](https://www.fiquipedia.es/licenciamiento/) y se almacenan pocos recursos que no sean de elaboración propia, de los que se intenta atribuir la autoría y citar fuente, y a los que suele aplicar [Artículo 32. Citas y reseñas e ilustración con fines educativos o de investigación científica de Real Decreto Legislativo 1/1996, de 12 de abril, por el que se aprueba el texto refundido de la Ley de Propiedad Intelectual, regularizando, aclarando y armonizando las disposiciones legales vigentes sobre la materia. ](https://www.boe.es/buscar/act.php?id=BOE-A-1996-8930#a32). Un ejemplo son recortes de vídeos de pocos segundos que había raelizado y tenía enlazados a Dropbox, que ahora guardo en drive.fiquipedia/media.  

# Imágenes

Antes migración Google Sites se almacenaban algunas imágenes estáticas en `_/rsrc/`, primero se mueven a fiquipedia/content/images, luego a drive.fiquipedia/content/images, y finalmente, una vez creada carpeta drive.fiquipedia/media, se colocan en drive.fiquipedia/media/images  
Se comentan los ficheros que había en `_/rsrc/` y algunas imágenes que se usan / almacenan. 

* Iconos ficheros:
Para pdf ntes de 2021 se usaba https://upload.wikimedia.org/wikipedia/commons/2/22/Pdf_icon.png  
En 2021 se usa https://upload.wikimedia.org/wikipedia/commons/8/87/PDF_file_icon.svg  
Una idea sería en todos los ficheros referenciar algo único (por ejemplo /images/iconopdf sin extensión) y así se actualizaría en un único sitio, aunque probado no funciona, quizá por extensión  

Para Word inicialmente se usaba http://upload.wikimedia.org/wikipedia/commons/thumb/8/88/MS_word_DOC_icon.svg/46px-MS_word_DOC_icon.svg.png  
En 2021 en la migración veo que no funciona y uso https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/MS_word_DOC_icon_(2003-2007).svg/46px-MS_word_DOC_icon_(2003-2007).svg.png   

Para escaneado se usaba https://upload.wikimedia.org/wikipedia/commons/b/b1/Crystal_Clear_mimetype_font_bitmap.png  

En principio se pasa a usar iconos unicode, uno único que indiquen documento en general https://unicode-table.com/en/1F4D6/ 📖   
Para abreviar tablas, se pone texto emergente, y cuando lo hay adicional/relevante respecto a información general se indica https://unicode-table.com/en/1F4AC/  💬  
Si es a un enlace, https://unicode-table.com/en/1F517/  🔗  

* Logo FiQuiPedia: 2017-06-23-LogoFiquipedia.png
Antes migración Google Sites lo almacenaba en _/rsrc/1498312812328/home/2017-06-23-LogoFiquipedia3eaf.png  
En migración tema hugo lo usa en themes/beautifulhugo/static/img/img/avatar-icon.png  
Se usa en config.toml desde [Params]  logo = "img/avatar-icon.png"

* Icono para web: favicon.ico
Antes migración Google Sites lo almacenaba en  _/rsrc/1528405774924/favicon.png  
En migración tema hugo lo usa en themes/beautifulhugo/static/img/favicon.ico  
Se usa en config.toml desde [Params]  favicon = "img/favicon.ico"

* Código QR FiQuiPedia: FiquipediaQR.png
Antes migración Google Sites lo almacenaba en _/rsrc/1411658790825/home/FiquipediaQRca9d.png  
Pasa a estar en /media/images/FiquipediaQR.png

* Logo Real Sociedad Española de Química 
Antes migración Google Sites lo almacenaba en _/rsrc/1466526534242/home/logo-rseq-20167d02.jpg  
En migración se usa inicialmente https://rseq.org/wp-content/uploads/2020/05/2020_logorseqlargo_transp.png  
Finalmente no se usa icono: se usaba solo en referencias y se cita imagen de tuit https://twitter.com/FiQuiPedia/status/1176563161455050753

* Captura dibujos animados Dennis Gnasher: ReaccionesQuímicasDennisGnasher.png
Antes migración Google Sites lo almacenaba en  _/rsrc/1609430371289/home/recursos/quimica/recursos-reacciones-y-calculos-estequiometricos/DennisGnasher.png

# Vídeos

Capítulo 42 del Pequeño Reino de Ben y Holly, "Nanny Plum And The Wise Old Elf Swap Jobs For One Whole Day"  
 [BosonHiggs-en.mpg](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/video/BosonHiggs-en.mpg)   
 [BosonHiggs-es.mpg](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/video/BosonHiggs-es.mpg)  
 En español dicen: "Supongo que desmontaría el quark del bosón de Higgs booleano del núcleo de la bios y reconectaría el..."  
